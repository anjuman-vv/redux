import { BUY_FOOD } from "./actionTypes"

const initialState = {
    numOfIceFoods: 10
  } 

const foodReducer = (state = initialState, action) =>{
    switch (action.type) {
        case BUY_FOOD: return {
          ...state,
          numOfIceFoods: state.numOfIceFoods - 1
        }
    
        default: return state
      }  
}

export default foodReducer