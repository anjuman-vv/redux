import { applyMiddleware, combineReducers, compose, legacy_createStore } from "redux";
import thunk from "redux-thunk";
import foodReducer from "./foods/reducer";
import productReducer from "./Product/reducer";
import userReducer from "./user/reduce";





const rootReducer = combineReducers({
    food : foodReducer,
    product : productReducer,
    user : userReducer
})

const composeEnhancers = window._REDUX_DEVTOOLS_EXTENSION_COMPOSE_ 
|| compose



 export const store = legacy_createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
)

