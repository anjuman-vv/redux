import { BUY_PRODUCT } from './actionTypes'


const initialState = {
  numOfProduct: 5
}

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case BUY_PRODUCT: return {
      ...state,
      numOfProduct: state.numOfProduct - action.payload
    }

    default: return state
  }
}

export default productReducer

