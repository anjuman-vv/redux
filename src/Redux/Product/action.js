import { BUY_PRODUCT } from './actionTypes'


export const buyProduct = (number = 1) => {
  return {
    type: BUY_PRODUCT,
    payload: number
  }
}
