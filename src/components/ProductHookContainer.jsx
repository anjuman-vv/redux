import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { buyProduct } from '../Redux/Product/action'


export default function ProductHooksContainer () {
  const numOfProduct = useSelector(state => state.cake.numOfProduct)
  const dispatch = useDispatch()
  return (
    <div>
      <h2>Number of Products - {numOfProduct} </h2>
      <button onClick={() => dispatch(buyProduct())}>Buy Cake</button>
    </div>
  )
}

 
