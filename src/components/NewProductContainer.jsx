import React, { useState } from 'react'
import { buyProduct } from '../Redux/Product/action';



export const mapStateToProps = state => {
  return {
    numOfProduct : state.cake.numOfProduct 
  }
} 

export const mapDispatchToProps = dispatch => {
  return {
    buyProduct: number => dispatch(buyProduct(number))
  }
} 



export default function NewProductContainer(props) {
  const [number, setNumber] = useState(1);


     return (
    <div>
        <h2>no. of Products - { props.numOfProducts }</h2>  
        <input type="text" value={number} onChange={e => setNumber(e.target.value)} />
        <button onClick={() => props.buyProduct(number)}>buy new products</button>
    </div>
  )
}



 

