
import React from 'react'
import { connect } from 'react-redux'
import { buyProduct } from '../Redux/Product/actio'

export default function ProductContainer (props) {
  return (
    <div>
      <h2>Number of products - {props.numOfProduct} </h2>
      <button onClick={props.buyProduct}>Buy products</button>
    </div>
  )
}

export const mapStateToProps = state => {
  return {
    numOfProduct: state.cake.numOfProduct
  }
}

 export const mapDispatchToProps = dispatch => {
  return {
    buyProduct: () => dispatch(buyProduct())
  }
}

